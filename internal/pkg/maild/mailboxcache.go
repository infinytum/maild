package maild

import "errors"

// MailboxCache enabled maild to cache mailbox instances to reduce database load
type MailboxCache struct {
	cache   map[string]*Mailbox
	service MailboxService
}

// NewMailboxCache returns a new read-through cache for service.
func NewMailboxCache(service MailboxService) *MailboxCache {
	return &MailboxCache{
		cache:   make(map[string]*Mailbox),
		service: service,
	}
}

// Mailbox returns a mailbox identified by given UUID.
// Returns from cache if available.
func (c *MailboxCache) Mailbox(uuid UUID) (*Mailbox, error) {
	if uuid == nil {
		return nil, errors.New("UUID of mailbox cannot be null")
	}

	// Return instance from cache if we got a hit
	if u := c.cache[uuid.String()]; u != nil {
		return u, nil
	}

	// Cache miss, fetch mailbox from service
	u, err := c.service.Mailbox(uuid)
	if err != nil {
		return nil, err
	} else if u != nil {
		c.cache[uuid.String()] = u
	}

	return u, err
}

// Expire removes a cached instance from the cache by given UUID.
// Returns the instance stored in the cache.
func (c *MailboxCache) Expire(uuid UUID) (*Mailbox, error) {
	if uuid == nil {
		return nil, errors.New("UUID of mailbox cannot be null")
	}

	// Return instance from cache if we got a hit
	if u := c.cache[uuid.String()]; u != nil {
		delete(c.cache, uuid.String())
		return u, nil
	}

	return nil, errors.New("UUID was not found in cache")
}

// Contains determines whether a mailbox is present in the cache
func (c *MailboxCache) Contains(uuid UUID) (bool, error) {
	if uuid == nil {
		return false, errors.New("UUID of mailbox cannot be null")
	}

	_, exists := c.cache[uuid.String()]
	return exists, nil
}

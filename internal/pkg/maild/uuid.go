package maild

// UUID represents a valid RFC 4122 compilant unique identifier string
type UUID interface {
	NewUUID() (*UUID, error)
	Parse(s string) (*UUID, error)
	String() string
}

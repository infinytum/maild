package google

import (
	guuid "github.com/google/uuid"
)

// UUID represents a valid RFC 4122 compilant unique identifier string
// UUID is implemented with google/uuid
type UUID struct {
	uuid guuid.UUID
}

// NewUUID generates a random UUID
func NewUUID() (*UUID, error) {
	uuid, err := guuid.NewRandom()

	if err != nil {
		return nil, err
	}

	return &UUID{
		uuid: uuid,
	}, nil
}

// Parse converts a string into an UUID object
func Parse(s string) (*UUID, error) {
	uuid, err := guuid.Parse(s)

	if err != nil {
		return nil, err
	}

	return &UUID{
		uuid: uuid,
	}, nil
}

// String returns the uuid as a string
func (u *UUID) String() string {
	return u.uuid.String()
}

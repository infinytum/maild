package maild

// Mailbox represents a mailbox owned by a user
type Mailbox struct {

	// Unique identifier for a mailbox
	UUID *UUID

	// Displayname of a mailbox instance
	Name string

	//Messages

	// Instance of owner
	Owner *User

	// Is client subscription active
	Subscribed bool
}

// NewMailbox initiates a new instance of the Mailbox struct.
func NewMailbox() (*Mailbox, error) {
	return &Mailbox{}, nil
}

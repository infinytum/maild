package maild

// User that can authenticate against maild
type User struct {

	// UUID is a unique identifier for this user
	UUID *UUID

	// Determines login state of an user instance
	Authenticate bool

	// Array of Mailboxes owned by this user
	Mailboxes map[string]*Mailbox

	// Username to authenticate against the maild server
	Username string
}

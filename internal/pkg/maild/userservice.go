package maild

// UserService resolves users identified by various identifiers
type UserService interface {
	User(uuid UUID) (*User, error)
	Users() ([]*User, error)
}

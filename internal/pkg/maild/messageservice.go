package maild

// MessageService resolves messages identified by various identifiers
type MessageService interface {
	Message(uuid UUID) (*Message, error)
	Messagees() ([]*Message, error)
}

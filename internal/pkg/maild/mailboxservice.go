package maild

// MailboxService resolves mailboxes identified by various identifiers
type MailboxService interface {
	Mailbox(uuid UUID) (*Mailbox, error)
	Mailboxes() ([]*Mailbox, error)
}

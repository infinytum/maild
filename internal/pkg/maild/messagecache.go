package maild

import "errors"

// MessageCache enabled maild to cache message instances to reduce database load
type MessageCache struct {
	cache   map[string]*Message
	service MessageService
}

// NewMessageCache returns a new read-through cache for service.
func NewMessageCache(service MessageService) *MessageCache {
	return &MessageCache{
		cache:   make(map[string]*Message),
		service: service,
	}
}

// Message returns a message identified by given UUID.
// Returns from cache if available.
func (c *MessageCache) Message(uuid UUID) (*Message, error) {
	if uuid == nil {
		return nil, errors.New("UUID of message cannot be null")
	}

	// Return instance from cache if we got a hit
	if u := c.cache[uuid.String()]; u != nil {
		return u, nil
	}

	// Cache miss, fetch message from service
	u, err := c.service.Message(uuid)
	if err != nil {
		return nil, err
	} else if u != nil {
		c.cache[uuid.String()] = u
	}

	return u, err
}

// Expire removes a cached instance from the cache by given UUID.
// Returns the instance stored in the cache.
func (c *MessageCache) Expire(uuid UUID) (*Message, error) {
	if uuid == nil {
		return nil, errors.New("UUID of message cannot be null")
	}

	// Return instance from cache if we got a hit
	if u := c.cache[uuid.String()]; u != nil {
		delete(c.cache, uuid.String())
		return u, nil
	}

	return nil, errors.New("UUID was not found in cache")
}

// Contains determines whether a message is present in the cache
func (c *MessageCache) Contains(uuid UUID) (bool, error) {
	if uuid == nil {
		return false, errors.New("UUID of message cannot be null")
	}

	_, exists := c.cache[uuid.String()]
	return exists, nil
}

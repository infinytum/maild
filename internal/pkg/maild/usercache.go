package maild

import (
	"errors"
)

// UserCache enabled maild to cache user instances to reduce database load
type UserCache struct {
	cache   map[string]*User
	service UserService
}

// NewUserCache returns a new read-through cache for service.
func NewUserCache(service UserService) *UserCache {
	return &UserCache{
		cache:   make(map[string]*User),
		service: service,
	}
}

// User returns a user identified by given UUID.
// Returns from cache if available.
func (c *UserCache) User(uuid UUID) (*User, error) {
	if uuid == nil {
		return nil, errors.New("UUID of user cannot be null")
	}

	// Return instance from cache if we got a hit
	if u := c.cache[uuid.String()]; u != nil {
		return u, nil
	}

	// Cache miss, fetch user from service
	u, err := c.service.User(uuid)
	if err != nil {
		return nil, err
	} else if u != nil {
		c.cache[uuid.String()] = u
	}

	return u, err
}

// Expire removes a cached instance from the cache by given UUID.
// Returns the instance stored in the cache.
func (c *UserCache) Expire(uuid UUID) (*User, error) {
	if uuid == nil {
		return nil, errors.New("UUID of user cannot be null")
	}

	// Return instance from cache if we got a hit
	if u := c.cache[uuid.String()]; u != nil {
		delete(c.cache, uuid.String())
		return u, nil
	}

	return nil, errors.New("UUID was not found in cache")
}

// Contains determines whether a user is present in the cache
func (c *UserCache) Contains(uuid UUID) (bool, error) {
	if uuid == nil {
		return false, errors.New("UUID of user cannot be null")
	}

	_, exists := c.cache[uuid.String()]
	return exists, nil
}

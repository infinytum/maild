package maild

import (
	"time"
)

// Message represents an e-mail stored in a mailbox
type Message struct {

	// Unique identifier of a message
	UUID *UUID

	// Plain-text message contents
	Body string

	// Date when this message was received
	Date time.Time

	// IMAP Flags
	Flags []string

	// Mailbox containing this message
	Mailbox *Mailbox

	// UID represents the IMAP sequence ID
	UID uint32
}
